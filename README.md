# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

#COMO COMPILAR:
*Para compilar esse projeto é necessário ter a IDE netbeans.
*Roda somente em um sistem operacional Linux

*1 - Baixe ou clone o arquivo do repositório.
*2 - Abre na IDE netbeans.
*3 - Execute o projeto (Run Project (F6)).

#TESTES:

*1 - Abra a pasta de teste
*2 - Execute o teste

#FUNCIONAMENTO:

*Ao compilar o codigo o programa irá abrir a tela de login, o mesmo só tem uma senha cadastrada:
*Login: teste
*senha: 123
