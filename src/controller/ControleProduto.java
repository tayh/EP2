/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dominio.Produto;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tayh
 */
public class ControleProduto {


    public void salvarProduto(Produto produto) throws IOException{
        String texto;
        File file = new File("src/view/bancoProduto.txt");
        
        
        BufferedWriter output = null;
        
        
        output = new BufferedWriter(new FileWriter(file, true));
        
        texto = String.valueOf(produto.getCodigo());
        output.write(texto + " ");
        
        texto = produto.getNome();
        output.write(texto + " ");

        texto = String.valueOf(produto.getPreco() + "");
        output.write(texto + " ");

        texto = String.valueOf(produto.getEstoque());
        output.write(texto + " ");

        texto = produto.getCategoria();
        output.write(texto + " " + "\n");

       
        if ( output != null ) {
            output.close();
        }
    
    }
    public void excluirProduto(String codigo, String nome) throws FileNotFoundException, IOException{
        Produto produto = new Produto();
        File file = new File("src/view/bancoProduto.txt");
        BufferedReader lerArquivo = new BufferedReader(new FileReader(file));
        
        String verificaArquivo = produto.getCodigo() + " " + produto.getNome() + " " 
        +produto.getPreco()+ " " +produto.getEstoque() +produto.getCategoria() + "\n";
        
        File copia = new File("src/view/bancoProdutoCopia.txt");
        
        BufferedWriter arquivo;
	arquivo = new BufferedWriter(new FileWriter(copia, true));
	
        String aux = lerArquivo.readLine();
        
        while(aux != null){
					
            if(verificaString(aux, codigo) || verificaString(aux, nome)){
                System.out.print("não faz nada");
                //produto qnt - qtde
            } else {
                String gravar = aux;
                arquivo.append(gravar);
            }
            aux = lerArquivo.readLine();
	}
        
        arquivo.close();	
	lerArquivo.close();
			
	file.delete();
	copia.renameTo(file);
    }
    public List<String> pegaArquivo() throws FileNotFoundException{
        FileInputStream file;
        List<String> lista = new ArrayList();
        file = new FileInputStream("src/view/bancoProduto.txt");
        int aux;
        char letra;
        try {
                while((aux = file.read()) != -1) {
                    letra = (char)aux;
                    lista.add(String.valueOf(letra));
                    System.out.print(letra);				
                }			
        } catch (IOException e) {
            
        } finally {
            if (file != null) {
                try {
                    file.close();
                } catch (IOException ex) {
                    Logger.getLogger(ControleProduto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return lista;
    } 

    private boolean verificaString(String arquivo, String tela) {
        if (tela.length() == 0) {
            return false;
        }
        return arquivo.toLowerCase().contains(tela.toLowerCase());
    }

     
}
