package dominio;

public class Cliente extends Pessoa{
    private int mesa;
    
    public Cliente(String nome) {
        super(nome);
    }

    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }
    
    
}
