package dominio;


public class Funcionario extends Pessoa{
    private String login;
    private String senha;
    
    public Funcionario(String nome){
        super(nome);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
