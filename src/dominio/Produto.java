/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

/**
 *
 * @author tayh
 */
public class Produto {
    
    private String nome;
    private Float preco;
    private Integer codigo;
    private Integer quantidade;
    private String categoria;
    private Integer estoque;
    
    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "Codigo: " + codigo + " Nome: " + nome +  " Quantidade: " + quantidade + " Preço: " + preco + " Categoria: " + categoria;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public Float getPreco() {
        return preco;
    }
    public void setPreco(Float preco) {
        this.preco = preco;
    }
    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getEstoque() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {
        this.estoque = estoque;
    }
    
    
}
