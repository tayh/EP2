/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import static org.junit.Assert.assertFalse;

import controller.ControleProduto;
import dominio.Produto;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

//import controlle.ControleProduto;
/**
 *
 * @author tayh
 */
public class TesteCadastroProduto{
    @Test
    
    public void deveCadastrarProduto() throws Exception{
        Produto produto = new Produto();
        
        produto.setCodigo(0);
        produto.setNome("macarrao");
        produto.setPreco(12.56f);
        produto.setEstoque(5);
        produto.setCategoria("alguma");
        
        assertEquals("123", produto.getCodigo());
        assertEquals("suco", produto.getNome());
        assertEquals("10.50", produto.getPreco());
        assertEquals("2", produto.getEstoque());
        assertEquals("bebida", produto.getCategoria());
        
    }
}
