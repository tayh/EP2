/*/
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import dominio.Pedido;
import java.util.Date;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author tayh
 */
public class TestePedido {
    public void deveCadastrarPedido(){
    Pedido pedido = new Pedido();
    pedido.setData(new Date());
    pedido.setTotalPreco(15.50f);
    pedido.setObservacao("assim");
    pedido.setMesa(5);
    
    assertEquals(new Date(), pedido.getData());
    assertEquals("12.6f", pedido.getTotalPreco());
    assertEquals("haha", pedido.getObservacao());
    assertEquals("2", pedido.getMesa());
    
    }
}
